# Reboot Checker
This repository contains a simple script which checks if a reboot of the
system is required or not. It's result is sent to libnotify.

## How it works
Execute the `check-reboot` script. It will lookup linux images in
`/boot/vmlinuz*` and determine it's kernel version. The currently
running kernel version is determined by `uname -r` command.

If both mismatch a notification is sent using `notify-send` command.

### Add a cronjob
Add a cronjob to periodically check if a reboot is required. Such a line in a
users crontab could look like
```
*/15 * * * * DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$UID/bus /usr/bin/check-reboot
```
to check for a required reboot every 15 minutes. The part
`DISPLAY=:0 DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$UID/bus` is necessary
to allow invoking notify-send from background scripts.
